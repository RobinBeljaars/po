const chai = require('chai')
const chaiHttp = require('chai-http')
const jwt = require('jsonwebtoken')
const logger = require('../src/config/appconfig').logger
const server = require('../app')
const endpointToTest = '/api/appartments'
const authorizationHeader = 'authorization'
let token
let remove

chai.should()
chai.use(chaiHttp)
// Deze functie wordt voorafgaand aan alle tests 1 x uitgevoerd.
before(() => {
	logger.info('- before: get valid token')
	// We hebben een token nodig om een ingelogde gebruiker te simuleren.
	// Hier kiezen we ervoor om een token voor UserId 1 te gebruiken.
	const payload = {
            UserId: 1
    }
	jwt.sign({ data: payload }, 'login', { expiresIn: 60 * 60 }, (err, result) => {
    if (result) {
		token = result
    }
	logger.debug(payload)
	})
})

describe('Appartment API create', () => {
	it('should return http 200 when creating a apartment with values', done => {
		// Write your test here
		chai
			.request(server)
			.post(endpointToTest)
			.set(authorizationHeader, 'Bearer ' + token)
			.send({
				description:"gezellige huis aan de rand van de stad",
				streetaddress: "abclaan 102",
				postalcode:"3314EC",
				city:"Dordrecht"
			})
			.end((err, res) => {
				res.should.have.status(200)
				res.body.should.be.a('object')
				const result = res.body.result
    
				const appartment = result[0]
				remove = appartment.ApartmentId
				logger.info(remove)
				appartment.should.have.property('ApartmentId')
				done()
			})
	})
})

describe('Appartment API GET', () => {
	it('should return an array of Appartment', done => {
		// Write your test here
		chai
			.request(server)
			.get(endpointToTest)
			.end((err, res) => {
				res.should.have.status(200)
				res.body.should.be.a('object')
				const result = res.body.result
				const appartment = result[0]
				appartment.should.have.property('ApartmentId')
				appartment.should.have.property('Description').that.is.a('string')
				appartment.should.have.property('AdressApartment').that.is.a('string')
				appartment.should.have.property('PostalCodeApartment').that.is.a('string')
				appartment.should.have.property('CityApartment').that.is.a('string')
				appartment.should.have.property('UserId')
				appartment.should.have.property('FirstName').that.is.a('string')
				appartment.should.have.property('LastName').that.is.a('string')
				appartment.should.have.property('DateOfBirth').that.is.a('string')
				appartment.should.have.property('PhoneNumber').that.is.a('string')
				appartment.should.have.property('EmailAddress').that.is.a('string')
				appartment.should.have.property('Address').that.is.a('string')
				appartment.should.have.property('OwnerPostalCode').that.is.a('string')
				appartment.should.have.property('ReservationId')
				appartment.should.have.property('StartDate').that.is.a('string')
				appartment.should.have.property('EndDate').that.is.a('string')
				appartment.should.have.property('Status').that.is.a('string')
				appartment.should.not.have.property('password')
				done()
			})
	})
})

describe('Appartment API PUT', () => {
	it('should return the updated Appartment when providing valid input', done => {
		// Write your test here
		const endpoint = "/api/appartments/" + remove
		chai
			.request(server)
			.put(endpoint)
			.set(authorizationHeader, 'Bearer ' + token)
			.send({
				description: 'Avans Hogeschoollaan',
				city: 'Breda',
				streetaddress: 'Lovensdijkstraat 61',
				postalcode: '4800RA'    
			})
			.end((err, res) => {
				res.should.have.status(200)
				res.body.should.be.a('object')
				const result = res.body.result
				result.should.be.an('array').that.has.length(1)
				const apartment = result[0]
				apartment.should.have.property('ApartmentId')
				apartment.should.have.property('Description').that.is.a('string')
				apartment.should.have.property('StreetAddress').that.is.a('string')
				apartment.should.have.property('PostalCode').that.is.a('string')
				apartment.should.have.property('City').that.is.a('string')
				apartment.should.not.have.property('password')
				done()
			})
	})
})

describe('Appartment API DELETE', () => {
	it('should return http 200 when deleting a apartment with existing id', done => {
		// Write your test here
		let endpoint = "/api/appartments/"
		endpoint = endpoint + remove
		logger.debug(endpoint)
		chai
			.request(server)
			.delete(endpoint)
			.set(authorizationHeader, 'Bearer ' + token)
			.end((err, res) => {
				res.should.have.status(200)
				res.body.should.be.a('object')
				const result = res.body.result
				const apartment = result
				apartment.should.have.property('rowsAffected')
				done()
			})
	})
})
