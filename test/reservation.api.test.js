const chai = require('chai')
const chaiHttp = require('chai-http')
const jwt = require('jsonwebtoken')
const logger = require('../src/config/appconfig').logger
const server = require('../app')
const endpointToTest = '/api/appartments/1/reservations'
const authorizationHeader = 'authorization'
const key = require('../src/config/appconfig').secretKey
let token
let remove

chai.should()
chai.use(chaiHttp)

before(() => {
  logger.info('- before: get valid token')
  // We hebben een token nodig om een ingelogde gebruiker te simuleren.
  // Hier kiezen we ervoor om een token voor UserId 1 te gebruiken.
  const payload = {
            UserId: 1
          }
  jwt.sign({ data: payload }, key , { expiresIn: 60 * 60 }, (err, result) => {
    if (result) {
		token = result
    }
	logger.debug(payload)
  })
})

describe('Reservation API create', () => {
	it('should return http 200 when creating a Reservation with values', done => {
		// Write your test here
		chai
			.request(server)
			.post(endpointToTest)
			.set(authorizationHeader, 'Bearer ' + token)
			.send({
				startdate:"2019-05-13",
				enddate:"2019-06-13"
			})
			.end((err, res) => {
				res.should.have.status(200)
				res.body.should.be.a('object')
				const result = res.body.result
    
				const reservering = result[0]
				remove = reservering.ReservationId
				logger.debug(remove)
				reservering.should.have.property('ReservationId')
				done()
			})
	})
})

describe('Reservation API GET', () => {
	it('should return an array of Reservation', done => {
		// Write your test here
		chai
			.request(server)
			.get(endpointToTest)
			.end((err, res) => {
				res.should.have.status(200)
				res.body.should.be.a('object')
				const result = res.body.result
				const reservering = result[0]
				reservering.should.have.property('ReservationId')
				reservering.should.have.property('ApartmentId')
				reservering.should.have.property('StartDate').that.is.a('string')
				reservering.should.have.property('EndDate').that.is.a('string')
				reservering.should.have.property('Status').that.is.a('string')
				reservering.should.have.property('UserId')
				done()
			})
	})
})

describe('Reservation API PUT', () => {
	it('should return the updated Reservation when providing valid input', done => {
		// Write your test here
		const endpoint = endpointToTest+"/" + remove
		chai
			.request(server)
			.put(endpoint)
			.set(authorizationHeader, 'Bearer ' + token)
			.send({
				status:"REJECTED"    
			})
			.end((err, res) => {
				res.should.have.status(200)
				res.body.should.be.a('object')
				const result = res.body.result
				result.should.be.an('array').that.has.length(1)
				const reservering = result[0]
				reservering.should.have.property('ReservationId')
				reservering.should.have.property('ApartmentId')
				reservering.should.have.property('StartDate').that.is.a('string')
				reservering.should.have.property('EndDate').that.is.a('string')
				reservering.should.have.property('Status').that.is.a('string')
				reservering.should.have.property('UserId')
				done()
			})
	})
	it('should not return the updated Reservation when providing invalid input', done => {
		// Write your test here
		const endpoint = endpointToTest+"/" + remove
		let dummytoken
		const payload = {
				UserId: 2
		}
		jwt.sign({ data: payload }, key, { expiresIn: 60 * 60 }, (err, result) => {
    
			if (result) {
				dummytoken = result
			}
			chai
				.request(server)
				.put(endpoint)
				.set(authorizationHeader, 'Bearer ' + dummytoken)
				.send({
					status:"REJECTED"	
				})
				.end((err, res) => {
					res.should.have.status(401)
					res.body.should.be.a('object')
					const result = res.body
					let validator = new RegExp('(Access denied.)')
					let validator2 = new RegExp('(401)')
					let tester = validator.test(result.message)
					let tester2 = validator2.test(result.code)
					tester.should.equal(true)
					tester2.should.equal(true)
					done()
				})
		})
	})
})
describe('Reservation API DELETE', () => {
	it('should return http 200 when deleting a Reservation with existing id', done => {
		// Write your test here
		let endpoint = "/api/appartments/1/reservations/"
		endpoint = endpoint + remove
		logger.debug(endpoint)
		chai
			.request(server)
			.delete(endpoint)
			.set(authorizationHeader, 'Bearer ' + token)
			.end((err, res) => {
				res.should.have.status(200)
				res.body.should.be.a('object')
				const result = res.body.result
				const reservering = result
				reservering.should.have.property('rowsAffected')
				done()
			})
	})
	it('should return error access denied', done => {
		// Write your test here
		const endpoint = endpointToTest+"/" + remove
		let dummytoken
		const payload = {
				UserId: 2
        }
		jwt.sign({ data: payload }, key, { expiresIn: 60 * 60 }, (err, result) => {
			if (result) {
				dummytoken = result
			}
			chai
				.request(server)
				.delete(endpoint)
				.set(authorizationHeader, 'Bearer ' + dummytoken)
				.end((err, res) => {
					res.should.have.status(401)
					res.body.should.be.a('object')	
					const result = res.body
					let validator = new RegExp('(Access denied.)')
					let validator2 = new RegExp('(401)')
					let tester = validator.test(result.message)
					let tester2 = validator2.test(result.code)
					tester.should.equal(true)
					tester2.should.equal(true)
					done()
				})
		})
	})	
})