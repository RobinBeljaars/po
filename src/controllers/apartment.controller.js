const logger = require('../config/appconfig').logger
const database = require('../datalayer/mssql.dao')
const assert = require('assert')

module.exports = {
	createApartment: (req, res, next) => {
		logger.info('Post /api/appartments aangeroepen')
		// Check dat we de UserId in het request hebben - vanuit de validateToken functie.
		logger.trace('User id:', req.userId)

		// hier komt in het request een Apartment binnen.
		const apartment = req.body
		logger.info(apartment)
		try {
			let validator = new RegExp('{\A[1-9][0-9]{3}([A-RT-Z][A-Z]|[S][BCE-RT-Z])\z}')
			let tester = validator.test(apartment.postalcode) ? 'WEL' : 'NIET'
			// Valideer hier de properties van de Apartment die we gaan maken.
			assert(tester, 'not a valid PostalCode')
			assert.equal(typeof apartment.description, 'string', 'apartment.description is required.')
			assert.equal(typeof apartment.streetaddress, 'string', 'street adress is required')
			assert.equal(typeof apartment.city, 'string', 'city is required')
	  
		} catch (e) {
			const errorObject = {
				message: e.toString(),
				code: 400
			}
			return next(errorObject)
		}
		const query = "INSERT INTO Apartment(Description, StreetAddress, PostalCode, City, UserId) VALUES ('" +
			apartment.description +
			"','" +
			apartment.streetaddress +
			"','" +
			apartment.postalcode +
			"','" +
			apartment.city +
			"','" +
			req.userId +
			"'); SELECT SCOPE_IDENTITY() AS ApartmentId"
		database.executeQuery(query, (err, rows) => {
			// verwerk error of result
			if (err) {
				const errorObject = {
					message: 'Er ging iets mis in de database.',
					code: 500
				}
				next(errorObject)
			}
			if (rows) {
				res.status(200).json({ result: rows.recordset })
			}
		})
	},
	getAllApartments: (req, res, next) => {
		logger.info('Get /api/appartments aangeroepen')
		const query =
			'SELECT A.ApartmentId, A.Description, A.StreetAddress As AdressApartment, A.PostalCode AS PostalCodeApartment,' +
			' A.City As CityApartment, A.UserId, U.FirstName, U.LastName, U.DateOfBirth, U.PhoneNumber, U.EmailAddress,' +
			' U.StreetAddress As Address, U.PostalCode AS OwnerPostalCode,' +
			' Res.ReservationId, Res.StartDate, Res.EndDate, Res.Status' +
			' FROM Apartment AS A ' +
			'INNER JOIN DBUser AS U ON A.UserId = U.UserId ' +
			'INNER JOIN Reservation As Res ON (A.ApartmentId = Res.ApartmentId)'
		database.executeQuery(query, (err, rows) => {
		// verwerk error of result
			if (err) {
				const errorObject = {
					message: 'Er ging iets mis in de database.',
					code: 500
				}
				next(errorObject)
			}
			if (rows) {
				res.status(200).json({ result: rows.recordset })
			}
		})
	},
	getApartmentById: (req, res, next) => {
		logger.info('Get /api/appatments/id aangeroepen')
		const id = req.params.ApartmentId
		const query = `SELECT * FROM Apartment WHERE ApartmentId=${id};`
		database.executeQuery(query, (err, rows) => {
			// verwerk error of result
			if (err) {
				const errorObject = {
					message: 'Er ging iets mis in de database.',
					code: 500
				}
				next(errorObject)
			}
			if (rows) {
				res.status(200).json({ result: rows.recordset })
			}
		})
	},
	deleteApartmentById: (req, res, next) => {
		logger.info('deleteById aangeroepen')
		const id = req.params.ApartmentId
		const userId = req.userId
		const query = `DELETE FROM Apartment WHERE ApartmentId=${id} AND UserId=${userId}`
		database.executeQuery(query, (err, rows) => {
			// verwerk error of result
			if (err) {
				logger.trace('Could not delete Apartment: ', err)
				const errorObject = {
					message: 'Er ging iets mis in de database.',
					code: 500
				}
				next(errorObject)
			}
			if (rows) {
				if (rows.rowsAffected[0] === 0) {
					// query ging goed, maar geen film met de gegeven ApartmentId EN userId.
					// -> retourneer een error!
					const msg = 'Apartment not found or you have no access to this Apartment!'
					logger.trace(msg)
					const errorObject = {
						message: msg,
						code: 401
					}
					next(errorObject)
				} else {
					res.status(200).json({ result: rows })
				}
			}
		})
	},
	UpdateApartmentInfo: (req, res, next) => {
		logger.info('Update /api/appatments/id aangeroepen')
		const id = req.params.ApartmentId
		const apartment = req.body
		const postalcode = '3314EC'
		try {
			let validator = new RegExp('^[1-9][0-9]{3}([A-RT-Z][A-Z]|[S][BCE-RT-Z])?')
			let tester = validator.test(apartment.postalcode) ? 'WEL' : 'NIET'
			// Valideer hier de properties van de Apartment die we gaan maken.
			assert(tester, 'not a valid PostalCode')
			assert.equal(typeof apartment.description, 'string', 'apartment.description is required.')
			assert.equal(typeof apartment.streetaddress, 'string', 'street adress is required')
			assert.equal(typeof apartment.city, 'string', 'city is required')  
		} catch (e) {
			const errorObject = {
			message: e.toString(),
			code: 400
			}
			return next(errorObject)
		}
		const query = `Update Apartment SET Description = '${apartment.description}',`+
			` StreetAddress = '${apartment.streetaddress}', PostalCode = '${apartment.postalcode}'`+
			`, City = '${apartment.city}' WHERE ApartmentId=${id};`+
			` SELECT * From Apartment WHERE ApartmentId ='${id}'`
		database.executeQuery(query, (err, rows) => {
			// verwerk error of result
			if (err) {
				const errorObject = {
					message: 'Er ging iets mis in de database.',
					code: 500
				}
				next(errorObject)
			}
			if (rows) {
				res.status(200).json({ result: rows.recordset })
			}
		})
	}
}