const logger = require('../config/appconfig').logger
const database = require('../datalayer/mssql.dao')
const assert = require('assert')

module.exports = {
	createReservation: (req, res, next) => {
		logger.info('Post /api/appartments aangeroepen')
		// Check dat we de UserId in het request hebben - vanuit de validateToken functie.
		logger.trace('User id:', req.userId)
		logger.trace('apartment id:', req.params.ApartmentId)
		// hier komt in het request een Apartment binnen.
		const reservation = req.body
		logger.info(reservation)
		try {
			// Valideer hier de properties van de Apartment die we gaan maken.
			let validator = new RegExp('^20[0-9]{2}( |-)[0-1][0-9]( |-)[0-3][0-9]$')
			let value = validator.test(reservation.startdate)
			let currentdate = new Date(reservation["startdate"])
			let newdate = new Date(reservation["enddte"])
			if(Date.parse(currentdate)>Date.parse(newdate)){
				assert(false, 'startdate greater than end date')
			}
			assert(value, 'Invalid starting date')
			value = validator.test(reservation.enddate)
			assert(value, 'invalid end date')
		} catch (e) {
			const errorObject = {
				message: e.toString(),
				code: 400
			}
			return next(errorObject)
		}
		const query =
			`INSERT INTO Reservation(ApartmentId, StartDate, EndDate, Status, UserId)`+
			` VALUES ('` +
			req.params.ApartmentId +
			"','" +
			reservation.startdate +
			"','" +
			reservation.enddate +
			"','INITIAL" +
			"','" +
			req.userId +
			"'); SELECT SCOPE_IDENTITY() AS ReservationId"
		database.executeQuery(query, (err, rows) => {
			// verwerk error of result
			if (err) {
				const errorObject = {
					message: 'Er ging iets mis in de database.',
					code: 500
				}
				next(errorObject)
			}
			if (rows) {
				res.status(200).json({ result: rows.recordset })
			}
		})
	},
	getAllReservation: (req, res, next) => {
		logger.info('Get /api/appartments aangeroepen')
		const query =`SELECT * From Reservation WHERE ApartmentId = ${req.params.ApartmentId}`
		database.executeQuery(query, (err, rows) => {
		// verwerk error of result
			if (err) {
				const errorObject = {
					message: 'Er ging iets mis in de database.',
					code: 500
				}
				next(errorObject)
			}
			if (rows) {
				res.status(200).json({ result: rows.recordset })
			}
		})
	},
	getReservationById: (req, res, next) => {
		logger.info('Get /api/appatments/id aangeroepen')
		const id = req.params.ApartmentId

		const query =`SELECT * From Reservation WHERE ApartmentId = ${req.params.ApartmentId}`+
			` AND ReservationId = ${req.params.ReservationId}`
		database.executeQuery(query, (err, rows) => {
			// verwerk error of result
			if (err) {
				const errorObject = {
					message: 'Er ging iets mis in de database.',
					code: 500
				}
				next(errorObject)
			}
			if (rows) {
				res.status(200).json({ result: rows.recordset })
			}
		})
	},
	removeReservation: (req, res, next) => {
		logger.info('deleteById aangeroepen')
		const id = req.params.ReservationId
		let userId = req.userId
		const query = `DELETE FROM Reservation WHERE Reservation.ReservationId=${id}`+
			` AND Reservation.UserId=${userId}; SELECT Reservation.*, Apartment.UserId AS OwnerId `+
			`From Reservation INNER JOIN Apartment ON Reservation.ApartmentId=Apartment.ApartmentId`
		database.executeQuery(query, (err, rows) => {
			// verwerk error of result
			if (err) {
				logger.trace('Could not delete Apartment: ', err)
				const errorObject = {
					message: 'Er ging iets mis in de database.',
					code: 500
				}
				next(errorObject)
			}
			if (rows) {
				if(rows.recordset[0].UserId == req.userId){
					res.status(200).json({ result: rows })
				} else{
					const errorObject = {
						message: 'Access denied.',
						code: 401
					}
					next(errorObject)
				}
        
			}
		})
	},
	setReservationStatus: (req, res, next) => {
		logger.info('Update /api/appatments/id/Reservation/ReservationId aangeroepen')
		const id = req.params.ReservationId
		const reservation = req.body
		try {
			let validator = new RegExp('(INITIAL|ACCEPTED|REJECTED)')
			let value = validator.test(reservation.status)
			assert(value, 'Invalid status')
	  
		} catch (e) {
			const errorObject = {
				message: e.toString(),
				code: 400
			}
			return next(errorObject)
		}
		//const isOwner 
		const query = `UPDATE Reservation SET Status = '${reservation.status}'` +
			` FROM Reservation AS R INNER JOIN Apartment AS A ON R.ApartmentId = A.ApartmentId WHERE R.ReservationId = ${id} AND A.UserId =${req.userId}; SELECT R.*, A.UserId AS OwnerId From Reservation AS R INNER JOIN Apartment AS a ON R.ApartmentId=a.ApartmentId WHERE R.ReservationId ='${id}'`
		database.executeQuery(query, (err, rows) => {
			// verwerk error of result
			if (err) {
				const errorObject = {
					message: 'Er ging iets mis in de database.',
					code: 500
				}
				next(errorObject)
			}
			if (rows) {
				if(rows.recordset[0].OwnerId == req.userId){
					res.status(200).json({ result: rows.recordset })
				} else{
					const errorObject = {
						message: 'Access denied.',
						code: 401
					}
					next(errorObject)
				}
			}
		})
    }
}