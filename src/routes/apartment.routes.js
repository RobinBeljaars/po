const express = require('express')
const router = express.Router({mergeParams: true})
const ApartmentController = require('../controllers/apartment.controller')
const AuthController = require('../controllers/authentication.controller')
const ReservationController = require('../controllers/reservation.controller')

// Lijst van Apartments
router.post('/', AuthController.validateToken, ApartmentController.createApartment)
router.get('/', ApartmentController.getAllApartments)
router.get('/:ApartmentId', ApartmentController.getApartmentById)
router.delete('/:ApartmentId', AuthController.validateToken, ApartmentController.deleteApartmentById)
router.put('/:ApartmentId', AuthController.validateToken, ApartmentController.UpdateApartmentInfo)
// lijst van reservations
router.post('/:ApartmentId/reservations', AuthController.validateToken, ReservationController.createReservation)
router.get('/:ApartmentId/Reservations', ReservationController.getAllReservation)
router.get('/:ApartmentId/reservations/:ReservationId', ReservationController.getReservationById)
router.put('/:ApartmentId/reservations/:ReservationId', AuthController.validateToken, ReservationController.setReservationStatus)
router.delete('/:ApartmentId/reservations/:ReservationId', AuthController.validateToken, ReservationController.removeReservation)

module.exports = router
