const sql = require('mssql')
const config = require('../config/appconfig')
const logger = config.logger
let dbconfig = config.dbconfig
logger.info(process.env.NODE_ENV)
if ( process.env.NODE_ENV === 'test' ) { 
	dbconfig = {
		user: 'progr4',
		password: 'password123',
		server: 'aei-sql.avans.nl',
		database: 'Prog4-Eindopdracht1',
		port: 1443
	}  
}

module.exports = {
	executeQuery: (query, callback) => {
		sql.connect(dbconfig, err => {
		// ... error checks
		if (err) {
			logger.error('Error connecting: ', err.toString())
			callback(err, null)
			sql.close()
		}
		if (!err) {
			// Query
			new sql.Request().query(query, (err, result) => {
			// ... error checks
				if (err) {
					logger.error('error', err.toString())
					callback(err, null)
					sql.close()
				}
				if (result) {
					// logger.info(result)
					// result.recordset.forEach(item => console.log(item.number))
					callback(null, result)
					sql.close()
				}
			})
		}
		})
	},
	dbconfig
}
